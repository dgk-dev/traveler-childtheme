(function (original) {
    jQuery.fn.clone = function () {
        var result = original.apply(this, arguments),
            my_textareas = this.find('textarea').add(this.filter('textarea')),
            result_textareas = result.find('textarea').add(result.filter('textarea')),
            my_selects = this.find('select').add(this.filter('select')),
            result_selects = result.find('select').add(result.filter('select'));

        for (var i = 0, l = my_textareas.length; i < l; ++i) jQuery(result_textareas[i]).val(jQuery(my_textareas[i]).val());
        for (var i = 0, l = my_selects.length; i < l; ++i) {
            for (var j = 0, m = my_selects[i].options.length; j < m; ++j) {
                if (my_selects[i].options[j].selected === true) {
                    result_selects[i].options[j].selected = true;
                }
            }
        }
        return result;
    };
})(jQuery.fn.clone);

jQuery(document).ready(function() {
    jQuery('.print-tour').click(function(e) {
        e.preventDefault();
        
        // set Tour targets to print
        var targets = [
            '.st-heading',
            '.sub-heading',
            '.form-head',
            '.st-tour-feature',
            '.st-overview',
            '.st-highlight',
            '.st-program',
            '.st-include',
            '.st-faq:not(.st-conditions)',
            '.st-map-wrapper',
            '.st-conditions',
        ];
        
        var w = window.open('', '_blank');
        
		var print_html = '<!DOCTYPE html><html><head><title>' + document.getElementsByTagName('title')[0].innerHTML + '</title>';
        print_html += '<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" />';
        print_html += '<link rel="stylesheet" type="text/css" href="' + footerGlobalObject.templateUrl + '/print.css" />';

        //build the blank page
        w.document.open();
        w.document.write( print_html + '</head><body><div class="text-center" id="loader"><div class="spinner-border text-dark" role="status"></div>Generando impresión</div><div id="main-print"></div></body></html>');
        w.document.close();

        // clone variables
        // get vars
        var bodyStyles = window.getComputedStyle(document.body);
        var mainColor = bodyStyles.getPropertyValue('--main-color');
        // set vars
        w.document.body.style.setProperty('--main-color',  mainColor);

        //create header
        var header =    '<table class="header"><tbody><tr>'+
                            '<td valign="middle" width="300" class="logo">'+
                                '<img src="'+footerGlobalObject.templateUrl+'/img/logo_gran_turismo.png" class="img-fluid" />'+
                            '</td>'+
                            '<td valign="middle">'+
                                '<p>'+
                                    '<strong>Tel:</strong> (52) 443 3240484 <br>'+
                                    '<strong>Email:</strong> gtviajes@granturismoviajes.com'+
                                '</p>'+
                                '<p>Av. Camelinas 3233, Interior 102/103, Colonia Las Américas, C.P. 58270, Morelia Mich., México</p>'+
                                '<p>facebook.com/GranTurismoViajes/</p>'+
                                '<p>'+window.location.href+'</p>'+
                            '<td>'+
                        '</tr></tbody></table>';
        
        var $mainPrint = jQuery(w.document.body).find('#main-print').hide();
        $mainPrint.append(header);
        
        var ua = window.navigator.userAgent;

        targets.forEach(function(target, index, arr){
            if(!jQuery( target ).length) return;
            //rot in hell IE
            if ( ua.indexOf("MSIE ") != -1) {
                //alert('MSIE - Craptastic');
                $mainPrint.append( jQuery( target ).clone( true ).html() );
            } else if ( ua.indexOf("Trident/") != -1) {
                //console.log('IE 11 - Trident');
                $mainPrint.append( jQuery( target ).clone( true ).html() );
            } else if ( ua.indexOf("Edge/") != -1 ){
                //console.log('IE 12 - Edge');
                //there is a bug in Edge where no nested elements can be appended.
                jQuery( target ).each(function(){
                    var s = jQuery.trim( jQuery( this ).clone( true ).html() );
                    //The follwing solution is brought to you by reviver.lt
                    jQuery( w.document.body ).append( "<div>" + s + "</div>" );
                });
            } else{
                //console.log('good browser');
                $mainPrint.append( jQuery( target ).clone( true ) );
            }
        });

        printIt();

		function printIt(){
            w.onload = function() {
                var iframe = jQuery(w.document).find('iframe');
                var pauseTime = 0;
                if (iframe.length) {
                    pauseTime = 3000;
                }
                setTimeout(function(){
                    jQuery(w.document.body).find('#loader').hide();
                    $mainPrint.show();
                    w.focus();
                    w.print();
                    setTimeout(
                        function() {
                            w.close()
                        }, 1000
                    );
                }, pauseTime)
            }
		}
	});
});
