(function ($) {
    /**
     * Prellenado de formulario de contacto de tour
     */
    var $tourContactForm = $('.gt-tour-contact-form');
    if ($tourContactForm.length){
        var tripName = $('#tmp_trip_name').val();
        var userName = $('#tmp_user_name').val();
        var userMail = $('#tmp_user_mail').val();
        var userPhone = $('#tmp_user_phone').val();
        $('.wpcf7-form-control#nombre').val(userName);
        $('.wpcf7-form-control#email').val(userMail);
        $('.wpcf7-form-control#tel').val(userPhone);
        $('.wpcf7-form-control#viaje').val(tripName);
    }

    $('.no-tour-data').parents('.wpb_column').hide();

    var $topbar = $('#header #topbar');
    var $header = $('#header .header');
    $(window).scroll(function () {
        if ($(document).scrollTop() > $topbar.height()) {
            $header.addClass('sticky-header');
        }
        else {
            $header.removeClass('sticky-header');
        }
    });
})(jQuery)