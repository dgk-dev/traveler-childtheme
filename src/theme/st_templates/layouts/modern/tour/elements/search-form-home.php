<?php
$result_page = st()->get_option('tours_search_result_page');
$class = 'col-lg-9';
$id = 'id="sticky-nav"';
$in_tab = true;
if (isset($in_tab)) {
    $class = 'col-lg-12';
    $id = '';
}
?>
<div class="row">
    <div class="<?php echo esc_attr($class); ?> tour-search-form-home">
        <div class="search-form" <?php echo ($id); ?>>
            <form action="<?php echo esc_url(get_the_permalink($result_page)); ?>" class="form" method="get">
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group keywords-form-field">
                            <label><?php echo __( 'Palabras clave', ST_TEXTDOMAIN ); ?></label>
                            <input type="text" name="item_name" class="form-control" id="tour-keywords">
                        </div>
                    </div>
                    <div class="col-md-4">

                        <div class="form-button">
                            <button class="btn btn-primary btn-search" type="submit"><?php echo __('Search', ST_TEXTDOMAIN); ?></button>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?php echo st()->load_template('layouts/modern/tour/elements/search/advanced', '') ?>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>