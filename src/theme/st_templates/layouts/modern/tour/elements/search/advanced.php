<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 13-11-2018
 * Time: 3:00 PM
 * Since: 1.0.0
 * Updated: 1.0.0
 */

if (!isset($position))
	$position = '';
?>
<div class="advance-item facilities st-icheck">
	<?php
	$tax = STInput::get('taxonomy');
	$in_facilities = [];
	$temp_facilities = '';
	if (!empty($tax)) {
		if (isset($tax['st_tour_type'])) {
			if (!empty($tax['st_tour_type'])) {
				$temp_facilities = $tax['st_tour_type'];
				$in_facilities = explode(',', $tax['st_tour_type']);
			}
		}
	}

	$facilities = get_terms(
		[
			'taxonomy'   => 'st_tour_type',
			'hide_empty' => false
		]
	);
	?>
	<div class="item-title">
		<h4>
			<?php
			$taxObj = get_taxonomy('st_tour_type');
			if ($taxObj) {
				$nameTax = $taxObj->labels->name;
				echo esc_html($nameTax);
			}
			?>
		</h4>
	</div>
	<div class="item-content">
		<div class="row">
			<div class="ovscroll" tabindex="1">
				<?php
				if (!is_wp_error($facilities)) {
					foreach ($facilities as $term) {
				?>
						<div class="<?php echo ($position == 'sidebar') ? 'col-lg-12' : 'col-lg-4 col-sm-6'; ?>">
							<div class="st-icheck-item">
								<label><?php echo esc_html($term->name); ?><input type="checkbox" name="" value="<?php echo esc_attr($term->term_id); ?>" <?php echo in_array($term->term_id, $in_facilities) ? 'checked' : ''; ?>><span class="checkmark fcheckbox"></span>
								</label></div>
						</div>
				<?php
					}
				}
				?>
			</div>
		</div>
	</div>
	<input type="hidden" class="data_taxonomy" name="taxonomy[st_tour_type]" value="<?php echo esc_attr($temp_facilities); ?>">
</div>