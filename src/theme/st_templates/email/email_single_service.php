<div class="owner-info st-sent-mail-customer">
    <?php echo st()->load_template( 'layouts/modern/common/loader' ); ?>
    <?php 
        $get_post_type = get_post_type();
        switch ($get_post_type) {
            case 'st_hotel':
                    $type_service = __('Hotel',ST_TEXTDOMAIN);
                break;
            case 'hotel_room':
                    $type_service = __('Hotel Room',ST_TEXTDOMAIN);
                break;
            case 'st_rental':
                    $type_service = __('Rental',ST_TEXTDOMAIN);
                break;
            case 'rental_room':
                    $type_service = __('Rental Room',ST_TEXTDOMAIN);
                break;
            case 'st_activity':
                    $type_service = __('Activity',ST_TEXTDOMAIN);
                break;
            case 'st_tours':
                    $type_service = __('Tours',ST_TEXTDOMAIN);
                break;
            case 'st_cars':
                    $type_service = __('Cars',ST_TEXTDOMAIN);
                break;
            default:
                # code...
                break;
        }
        $current_user = wp_get_current_user();
        $user_meta = get_user_meta($current_user->ID);
    ?>
    <div class="media form-st-send-mail">
        <input type="hidden" id="tmp_service" value="<?php echo esc_attr($type_service);?>">
        <input type="hidden" id="tmp_trip_name" value="<?php the_title();?>">
        <input type="hidden" id="tmp_user_name" value="<?php echo $current_user->display_name ? $current_user->display_name : '' ?>">
        <input type="hidden" id="tmp_user_mail" value="<?php echo $current_user->user_email ? $current_user->user_email : '' ?>">
        <input type="hidden" id="tmp_user_phone" value="<?php echo isset( $user_meta['st_phone'] ) ? $user_meta['st_phone'][0] : '' ?>">
        <?php echo do_shortcode('[contact-form-7 title="Contacto Viajes"]'); ?>
    </div>
    <div class="text-center visible-md-block visible-lg-block">
        <button class="btn btn-primary btn-green upper font-small print-tour">Imprimir Tour</button>
    </div>
</div>