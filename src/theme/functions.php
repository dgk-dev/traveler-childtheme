<?php

// INCLUDES
//resources: CSS JS FONTS
require(get_stylesheet_directory().'/includes/resources.php');

//Visual Composer changes
require(get_stylesheet_directory().'/includes/vc-changes.php');

//Theme changes
require(get_stylesheet_directory().'/includes/theme-changes.php');

//Login styling
require(get_stylesheet_directory().'/includes/login-styling.php');


//Whatsapp
add_action('wp_footer','gt_add_footer_whatsapp');

function gt_add_footer_whatsapp(){
	$tel = "5214431094853 ";

	$url = "https://wa.me/${tel}";
	$img = get_stylesheet_directory_uri().'/img/whatsapp-icon.svg';
	echo "<div id='float-whatsapp' style='position:fixed;bottom:30px;right:30px;z-index:100;'>";
	echo " <a href=${url} target='_blank'>";
	echo " <img src='${img}' width=40 height=40 />";
	echo " </a>";
	echo "</div>";
}