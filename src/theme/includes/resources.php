<?php
function gt_resources(){
    $parent_style = 'traveler-style';
    wp_enqueue_style($parent_style, get_template_directory_uri() . '/style.css');
    wp_enqueue_style(
        'traveler-child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array($parent_style, 'responsive-css', 'enquire-css'),
        1
    );

    wp_enqueue_script('footer-bundle', get_stylesheet_directory_uri().'/js/footer-bundle.js', array('jquery'), null, true);
    wp_localize_script('footer-bundle', 'footerGlobalObject', array(
        'templateUrl' => get_stylesheet_directory_uri()
    ));
}
add_action('wp_enqueue_scripts', 'gt_resources');

/**
 * Change default gravatar.
 */

add_filter( 'avatar_defaults', 'gt_new_gravatar' );
function gt_new_gravatar ($avatar_defaults) {
    $gtAvatar = get_stylesheet_directory_uri().'/img/gt-avatar.png';
    $avatar_defaults[$gtAvatar] = "Gran Turismo Avatar";
    return $avatar_defaults;
}

//Google analytics tag
add_action( 'wp_head', 'gt_google_analytics', 7 );
function gt_google_analytics() {
    ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-174542153-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-174542153-1');
    </script>
    <?php
}