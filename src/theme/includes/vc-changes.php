<?php
// Cambio para aceptar categorías de Tour (st_tour_type) en shortcode de lista de servicios
st_reg_shortcode('st_list_of_services_new', 'st_list_of_services_new');
function st_list_of_services_new($attr, $content = false){
    $attr = shortcode_atts([
        'service' => 'st_hotel',
        'ids' => '',
        'posts_per_page' => 8,
        'style' => 'style1',
        'title' => '',
        'description' => '',
        'st_tour_type' => '',
        'show_featured' => ''
    ], $attr);

    return st()->load_template('layouts/modern/elements/list_of_service', '', $attr);
}

//Reescribe elementos de Visual Composer para agregar nuevos campos
add_action('vc_before_init', 'customLoadVCMapNewLayout', 15);
function customLoadVCMapNewLayout(){
    //Agrega el parámetro st_tour_type al elemento ST List of Services para elegir las categorías de un tour a través de su slug
    vc_map([
        'name' => __('ST List of Services', ST_TEXTDOMAIN),
        'base' => 'st_list_of_services_new',
        'icon' => 'icon-st',
        'category' => 'Modern Layout',
        'params' => [
            [
                'type' => 'dropdown',
                'heading' => __('Service', ST_TEXTDOMAIN),
                'param_name' => 'service',
                'value' => [
                    __('Hotel', ST_TEXTDOMAIN) => 'st_hotel',
                    __('Rental', ST_TEXTDOMAIN) => 'st_rental',
                    __('Tour', ST_TEXTDOMAIN) => 'st_tours',
                    __('Activity', ST_TEXTDOMAIN) => 'st_activity',
                    __('Car', ST_TEXTDOMAIN) => 'st_cars',
                ],
                'std' => 'st_hotel'
            ],
            [
                'type' => 'dropdown',
                'heading' => __('Style',ST_TEXTDOMAIN),
                'param_name' =>'style',
                'value' => [
                    __('Style 1',ST_TEXTDOMAIN) => 'style1',
                    __('Style 2',ST_TEXTDOMAIN) => 'style2',
                    __('Style 3',ST_TEXTDOMAIN) => 'style3',
                    __('Style 4',ST_TEXTDOMAIN) => 'style4',
                    __('Style 5',ST_TEXTDOMAIN) => 'style5',
                    __('Style 6',ST_TEXTDOMAIN) => 'style6',
                ],
                'std' => 'style1',
                'dependency' => array(
                    'element' => 'service',
                    'value' => array('st_tours')
                ),

            ],
            [
                'type' => 'textfield',
                'param_name' =>'title',
                'heading' => __('Title', ST_TEXTDOMAIN),
                'dependency' => array(
                    'element' => 'style',
                    'value' => array('style5', 'style1')
                ),

            ],
            [
                'type' => 'textarea',
                'param_name' =>'description',
                'heading' => __('Description', ST_TEXTDOMAIN),
                'dependency' => array(
                    'element' => 'style',
                    'value' => array('style5')
                ),
            ],
            [
                'type' => 'textfield',
                'param_name' => 'st_tour_type',
                'heading' => __('Tour Categories', ST_TEXTDOMAIN),
                'description' => __('Tour Category slugs separated by commas.', ST_TEXTDOMAIN),
                'dependency' => array(
                    'element' => 'service',
                    'value' => array('st_tours')
                ),
            ],
            [
                'type' => 'textfield',
                'param_name' => 'ids',
                'heading' => __('Service ID', ST_TEXTDOMAIN),
                'description' => __('Ids separated by commas. Example: 123,456', ST_TEXTDOMAIN)
            ],
            [
                'type' => 'textfield',
                'param_name' => 'posts_per_page',
                'heading' => __('Number of Items', ST_TEXTDOMAIN),
                'description' => __('-1 for unlimited', ST_TEXTDOMAIN),
                'std' => 8
            ],
            [
                'type' => 'checkbox',
                'param_name' => 'show_featured',
                'heading' => __('Show featured tours', ST_TEXTDOMAIN),
                'description' => __('', ST_TEXTDOMAIN),
            ],
        ]
    ]);

    //FAQs
    vc_map([
        "name" => __("ST FAQs", ST_TEXTDOMAIN),
        "base" => "st_faq_new",
        "icon" => "icon-st",
        "content_element" => true,
        "category" => "Modern Layout",
        "params" => array(
            [
                "type" => "textfield",
                "heading" => __("Title", ST_TEXTDOMAIN),
                "param_name" => "title",
                "description" => "",
            ],
            [
                'type' => 'param_group',
                'heading' => esc_html__('List items', ST_TEXTDOMAIN),
                'param_name' => 'list_faq',
                'value' => '',
                'params' => array(
                    [
                        "type" => "textfield",
                        "heading" => __("Title", ST_TEXTDOMAIN),
                        "param_name" => "title",
                    ],
                    [
                        "type" => "textarea_raw_html",
                        "heading" => __("Content", ST_TEXTDOMAIN),
                        "param_name" => "content",
                    ],
                ),
            ]
        )
    ]);
}
