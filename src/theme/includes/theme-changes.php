<?php

/**
 * Sobreescribir envio de email de consulta de viaje
 */
remove_action( 'wp_ajax_st_send_email_single_service', 'st_send_email_single_service',15 );
remove_action( 'wp_ajax_nopriv_st_send_email_single_service','st_send_email_single_service', 15 );

add_action( 'wp_ajax_st_send_email_single_service', 'gt_st_send_email_single_service' );
add_action( 'wp_ajax_nopriv_st_send_email_single_service','gt_st_send_email_single_service' );
function gt_st_send_email_single_service(){
    $type_service = isset($_GET['type_service']) ? $_GET['type_service'] : '';
    $name_service = isset($_GET['name_service']) ? $_GET['name_service'] : '';
    $name_st = isset($_GET['name_st']) ? $_GET['name_st'] : '';
    $email_st = isset($_GET['email_st']) ? $_GET['email_st'] : '';
    $phone_st = isset($_GET['phone_st']) ? $_GET['phone_st'] : '';
    $content_st = isset($_GET['content_st']) ? $_GET['content_st'] : '';
    $email_owl = isset($_GET['email_owl']) ? $_GET['email_owl'] : '';
    $status = 0;
    $message ='';
    $subject_st = $name_st.' - '.$email_st;
    if(empty($name_st) || (empty($email_st)) || !filter_var($email_st, FILTER_VALIDATE_EMAIL) || (empty($phone_st)) || !filter_var($phone_st, FILTER_SANITIZE_NUMBER_INT) || (empty($content_st))){
        $status = 0;
        if(empty($name_st)){
            $message .= __('Please enter your name',ST_TEXTDOMAIN).'<br>';
        }
        if(empty($email_st)){
            $message .= __('Please enter your mail',ST_TEXTDOMAIN).'<br>';
        }
        if (!empty($email_st) && !filter_var($email_st, FILTER_VALIDATE_EMAIL)) {
            $message .= 'Por favor ingresa una dirección de correo válida<br>';
        }
        if(empty($phone_st)){
            $message .= __('Please enter your phone',ST_TEXTDOMAIN).'<br>';
        }
        if(!empty($phone_st) && !filter_var($phone_st, FILTER_SANITIZE_NUMBER_INT)){
            $message .= 'Por favor ingresa un número de teléfono válido<br>';
        }
        if(empty($content_st)){
            $message .= __('Please enter your content',ST_TEXTDOMAIN).'<br>';
        }
        echo json_encode(
            array(
                'status' => $status,
                'message' => $message,
                )
            );
    } else{
        $status = 1;
        $admin_email      = st()->get_option( 'email_admin_address' );
        $body_email = "<html><body><h2>".$subject_st."</h2>";
        $body_email .= "<h3>ha enviado un mensaje de consulta:</h3>";
        $body_email .=  //"<strong>".__('Type Service',ST_TEXTDOMAIN)."</strong>: ".$type_service."<br/>".
                        "<strong>Nombre del servicio</strong>: ".$name_service."<br/>".
                        "<strong>Nombre del cliente</strong>: ".$name_st."<br/>".
                        "<strong>Email del cliente</strong>: ".$email_st."<br/>".
                        "<strong>Teléfono del cliente</strong>: ".$phone_st."<br/>".
                        "<strong>Mensaje</strong>: ".$content_st."<br/></html></body>";


        $multiple_to_recipients = array($email_owl,$email_st,$admin_email);
        $subject = 'Consulta acerca de un paquete de viaje desde '.home_url();
        $body    = $body_email;
        $headers = __('From:', ST_TEXTDOMAIN) . $name_st . "<" . $email_st .">" . "\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
        $headers .= 'Cc: '.$admin_email;
        $attachment = false;
        add_filter('wp_mail_content_type', 'set_html_content_type_sent_email');
        $check = wp_mail($multiple_to_recipients, $subject, $body, $headers,$attachment);
        remove_filter('wp_mail_content_type', 'set_html_content_type_sent_email');
        echo json_encode(
            array(
                'status' => $status,
                'message' => 'Gracias por contactarnos<br>Nos pondremos en contacto contigo de inmediato.',
                )
            );
    }
    die();
}

/**
 * Agregar caja "Opciones GRAN TURISMO VIAJES* a tour
 */

// Metabox conditions
add_action( 'add_meta_boxes', 'gt_tours_settings_metabox' );
function gt_tours_settings_metabox() {
    add_meta_box(
        'gt-tours-condition-metabox',
        __( 'Opciones GRAN TURISMO VIAJES', 'dgk-theme' ),
        'gt_tours_settings_metabox_callback',
        'st_tours',
        'advanced',
        'high'
    );
}

function gt_tours_settings_metabox_callback($post) {          
    wp_nonce_field( basename( __FILE__ ), 'tours_conditions_nonce' );
    $conditions = get_post_meta($post->ID, 'tours_conditions', true);
    $hide_price = get_post_meta( $post->ID, 'tours_hide_price',true );
    $featured = get_post_meta( $post->ID, 'tours_featured',true );
    ?>
    <table class="form-table">
        <tbody>
            <tr>
                <th>
                    Ocultar precio
                </th>
                <td>
                    <label for="tours_hide_price">
                        <input type="checkbox" name="tours_hide_price" id="tours_hide_price" <?php echo $hide_price ? 'checked="checked"' : '' ?> value="1">
                        Ocultar
                    </label>
                </td>
            </tr>
            <tr>
                <th>
                    Recomendado
                </th>
                <td>
                    <label for="tours_featured">
                        <input type="checkbox" name="tours_featured" id="tours_featured" <?php echo $featured ? 'checked="checked"' : '' ?> value="1">
                        Mostrar en Top Destinos
                    </label>
                </td>
            </tr>
        </tbody>
    </table>
    <hr>
    <h3>Condiciones generales</h3>
    <?php
        wp_editor ( 
            $conditions , 
            'tours_conditions', 
            array ( "media_buttons" => true ) 
        );
    ?>
    <?php
}

//Save conditions metabox data
add_action( 'save_post', 'gt_save_tours_conditions' );
function gt_save_tours_conditions( $post_id ) {
    // verify nonce
    if (!wp_verify_nonce($_POST['tours_conditions_nonce'], basename(__FILE__)))
        return $post_id;

    $oldconditions = get_post_meta($post_id, 'tours_conditions', true);
    $newconditions = $_POST['tours_conditions']; 
    if ($newconditions != $oldconditions) {
        update_post_meta($post_id, 'tours_conditions', $newconditions);
    }

    $oldhideprice = get_post_meta($post_id, 'tours_hide_price', true);
    $newhideprice = $_POST['tours_hide_price']; 
    if ($newhideprice != $oldhideprice) {
        update_post_meta($post_id, 'tours_hide_price', $newhideprice);
    }

    $oldfeatured = get_post_meta($post_id, 'tours_featured', true);
    $newfeatured = $_POST['tours_featured']; 
    if ($newfeatured != $oldfeatured) {
        update_post_meta($post_id, 'tours_featured', $newfeatured);
    }
}