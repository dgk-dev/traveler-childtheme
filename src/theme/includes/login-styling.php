<?php

/**
 * Custom login screen
 */
add_action('wp_dashboard_setup', 'gt_dashboard_widgets');
function gt_dashboard_widgets() {
    global $wp_meta_boxes;
    wp_add_dashboard_widget('custom_help_widget', 'Bienvenido', 'custom_dashboard_message');
}
 
function custom_dashboard_message() {
    echo '<div style="padding:20px 0; text-align:center;"><img src="'.get_stylesheet_directory_uri().'/img/logo_gt.svg"></div><p><strong>Este es el administrador del sitio PenPaper</strong></p><p>Desde aquí podrás gestionar las entradas de blog así como consultar y exportar la información de los usuarios que llenaron el formulario de contacto<p><p>En la sección <strong>Perfil</strong> podrás editar tus datos principales y tu contraseña.';
}

add_action( 'login_enqueue_scripts', 'gt_login_logo' );
function gt_login_logo() { ?>
    <style type="text/css">
        body.login{
            background: whitesmoke;
        }
        #login h1 a, .login h1 a {
            background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/logo_gt.svg');
            height: 162px;
            width: 300px;
            background-size: 300px 162px;
            background-repeat: no-repeat;
        	padding-bottom: 1em;
        }
        input[type="checkbox"]:focus, input[type="color"]:focus, input[type="date"]:focus, input[type="datetime-local"]:focus, input[type="datetime"]:focus, input[type="email"]:focus, input[type="month"]:focus, input[type="number"]:focus, input[type="password"]:focus, input[type="radio"]:focus, input[type="search"]:focus, input[type="tel"]:focus, input[type="text"]:focus, input[type="time"]:focus, input[type="url"]:focus, input[type="week"]:focus, select:focus, textarea:focus {
            border-color: #a81982 !important;
            box-shadow: 0 0 0 1px #a81982 !important;
            outline: 2px solid transparent;
        }
        .login form{
            border: 1px solid #008e8d !important;
        }
        .wp-core-ui .button-primary{
            background: #a81982 !important;
            border-color: #a81982 !important;
        }
    </style>
<?php }

add_filter( 'login_headerurl', 'gt_loginlogo_url');
function gt_loginlogo_url($url) {
    return home_url();
}

function gt_lost_password_redirect() {
    wp_redirect( home_url() ); 
    exit;
}
add_action('after_password_reset', 'gt_lost_password_redirect');

function gt_admin_login_redirect( $redirect_to, $request, $user ){
    global $user;
    if( isset( $user->roles ) && is_array( $user->roles ) ) {
        if( in_array( 'administrator', $user->roles ) ) {
            return $redirect_to;
        }else{
            return home_url();
        }
    }else{
        return $redirect_to;
    }
}
add_filter('login_redirect', 'gt_admin_login_redirect', 10, 3);